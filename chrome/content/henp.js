function henp_update() {
	const preferencesService = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("");

	var henpEnabled = preferencesService.getBoolPref("henp.enabled");
	var targetTime = preferencesService.getCharPref("henp.targetTime");
	var henpPanel = document.getElementById('henp_panel');
	
	if(!henpEnabled || !targetTime) {
		var disabledMessage = henpGetLocaleString('disabledMessage');
		henpPanel.className = 'disabled';
		henpPanel.firstChild.textContent = disabledMessage;
		return false;
	} else {
		henpPanel.className = '';
	}
	
	var hourPanel = document.getElementById('henp_hour_panel');
	var hourPanelTime = new Date();
	hourPanelTime.setTime(targetTime);
	hourPanelTime.setMinutes(hourPanelTime.getMinutes() - 30);
	hourPanel.textContent = henpGetLocaleString('happyHour') + ' ' + hourPanelTime.getHours() + ':' + hourPanelTime.getMinutes();

	var now = new Date();
	var timeToDeadline = Math.ceil((targetTime - now.getTime()) / 1000);
	var firstAlertTime = timeToDeadline - (35 * 60);
	var minimumTime = timeToDeadline - (30 * 60);
	var exactTime = timeToDeadline - (15 * 60);
	var lastAlertTime = timeToDeadline - (5 * 60);
	
	var timeLeftString = "" ;

	if(minimumTime >= 3600) { // Has at least 1 hour
		var hoursLeft = Math.floor( minimumTime / 3600 ) ;
		if(hoursLeft < 10) hoursLeft = "0" + hoursLeft ;
		minimumTime %= 3600 ;
		timeLeftString += hoursLeft + "h " ;
	}

	if(minimumTime > 60) { //Has at least 1 minutes
		var minutesLeft = Math.floor( minimumTime / 60 ) + 1;
		if(minutesLeft < 10) minutesLeft = "0" + minutesLeft ;
		minimumTime %= 60 ;
		timeLeftString += minutesLeft + "m " ;
	} else if(minimumTime > 1) { //seconds counter
		var secondsLeft = minimumTime;
		if(secondsLeft < 10) secondsLeft = "0" + secondsLeft;
		timeLeftString += secondsLeft + "s " ;
	} else {
		timeLeftString = '0';
	}
	timeLeftString = timeLeftString.replace(/\s$/,'') ;
	
	if(timeToDeadline >= 0) { // Display time left
		var countdownPanel = document.getElementById('henp_countdown_panel');
		countdownPanel.textContent = henpGetLocaleString('remaining') + ' ' + timeLeftString ;
		
		var alertIndex = preferencesService.getIntPref("henp.alertIndex");
		
		if(timeToDeadline < (timeToDeadline - lastAlertTime)) {
			henpPanel.className = 'lastAlertTime';
			if(alertIndex < 4) {
				alertIndex = henp_alert_status(4);
			}
		} else if(timeToDeadline < (timeToDeadline - exactTime)) {
			henpPanel.className = 'exactTime';
			if(alertIndex < 3) {
				alertIndex = henp_alert_status(3);
			}
		} else if(timeToDeadline < (timeToDeadline - minimumTime)) {
			henpPanel.className = 'minimumTime';
			if(alertIndex < 2) {
				alertIndex = henp_alert_status(2);
			}
		} else if(timeToDeadline < (timeToDeadline - firstAlertTime)) {
			henpPanel.className = 'first';
			if(alertIndex < 1) {
				alertIndex = henp_alert_status(1);
			}
		}
		preferencesService.setIntPref("henp.alertIndex", alertIndex);
		
	} else { // Time's up
		preferencesService.setBoolPref("henp.enabled", false);
		preferencesService.setIntPref("henp.alertIndex", 100);
		henp_alert_status(5);
	}
}

function henp_alert_status(alertIndex) {
	const preferencesService = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("");
	
	if(preferencesService.getBoolPref("henp.alertMethodSound")) {
		henp_play_file();
	}
	
	var promptString = henpGetLocaleString('alertMessage' + alertIndex) + henpGetLocaleString('alertFooter');
	if(confirm(promptString)){
		preferencesService.setIntPref("henp.alertIndex", 100);
	};
	
	return alertIndex;
}

function henp_play_file(){
		const preferencesService = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("");
		
		var player = Components.classes["@mozilla.org/sound;1"].getService(Components.interfaces.nsISound) ;
		var ios = Components.classes["@mozilla.org/network/io-service;1"].getService(Components.interfaces.nsIIOService) ;
		var file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile) ;
		try
		{
			file.initWithPath(preferencesService.getCharPref("henp.alertSoundFile")) ;
			player.play(ios.newFileURI(file)) ;
		}
		catch(e){}
}

function henpGetLocaleString(strName) {
	var str = null;
	try {
		var strbundle = document.getElementById('henp_strings');
		str = strbundle.getString(strName);
	} catch (e) {}
	return str;
}

/***** Options screen *****/
function loadTargetForm() {
	const preferencesService = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("");
	
	var initialTime = preferencesService.getCharPref("henp.initialTime").split(":") ;
	document.getElementById("spinHours").value = initialTime[0];
	document.getElementById("spinMinutes").value = initialTime[1];
	document.getElementById("holidaySaturday").checked = preferencesService.getBoolPref("henp.holidaySaturday");
	document.getElementById("alertMethodSound").checked = preferencesService.getBoolPref("henp.alertMethodSound") ;
	document.getElementById("alertSoundFile").value = preferencesService.getCharPref("henp.alertSoundFile") ;
}

function saveHenpSettings() {
	const preferencesService = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("");

	preferencesService.setCharPref("henp.targetTime", calculateTimes());

	var alertMethodSound = document.getElementById("alertMethodSound") ;
	var alertSoundFile = document.getElementById("alertSoundFile") ;
	var holidaySaturday = document.getElementById("holidaySaturday");
	preferencesService.setBoolPref("henp.holidaySaturday", holidaySaturday.checked) ;
	preferencesService.setBoolPref("henp.alertMethodSound", alertMethodSound.checked) ;
	preferencesService.setCharPref("henp.alertSoundFile", alertSoundFile.value) ;
	
	var initialHour = document.getElementById('spinHours').value;
	var initialMinute = document.getElementById('spinMinutes').value;
	preferencesService.setCharPref("henp.initialTime", initialHour + ':' + initialMinute) ;
	preferencesService.setBoolPref("henp.enabled", true);
	preferencesService.setIntPref("henp.alertIndex", 0);

	return true ;
}

function calculateTimes() {
	var initHour = parseInt(document.getElementById("spinHours").value,10) ;
	var initMinutes = parseInt(document.getElementById("spinMinutes").value,10) ;
	
	var initDate = new Date();
	initDate.setHours(initHour);
	initDate.setMinutes(initMinutes);
	initDate.setSeconds(0);
	initDate.setMilliseconds(0);
	
	var holidayWeek = document.getElementById("holidaySaturday").checked;
	var workHours = 9;
	var workMinutes = holidayWeek ? 0 : 48;
	var maxLimitMinutes = 15;

	var relativeTotalSeconds = (workHours * 3600) + (workMinutes * 60) + (maxLimitMinutes * 60);

	var relativeTargetTime = new Date() ;
	var relativeNumberOfSeconds = initDate.getTime() + (relativeTotalSeconds * 1000);
	relativeTargetTime.setTime(relativeNumberOfSeconds) ;

	//return (relativeTargetTime.getYear() + 1900)+":"+(relativeTargetTime.getMonth() + 1)+":"+relativeTargetTime.getDate()+":"+relativeTargetTime.getHours()+":"+relativeTargetTime.getMinutes()+":"+relativeTargetTime.getSeconds()
	return relativeTargetTime.getTime();
	
}

function browseSoundFile()
{
	try
	{
		var NFP = Components.interfaces.nsIFilePicker;
		var picker = Components.classes["@mozilla.org/filepicker;1"].createInstance(NFP);
		picker.init(window, null, NFP.modeOpen);
		picker.appendFilter("*.wav", "*.wav");
		if(picker.show() == NFP.returnOK)
		{
			document.getElementById("alertSoundFile").value = picker.file.path ;
		}
	}
	catch(e)
	{
		alert(e) ;
	}
}
